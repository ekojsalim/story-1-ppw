module.exports = {
  theme: {
    extend: {},
    fontFamily: {
      heading: ["Poppins", "-apple-system", "BlinkMacSystemFont", "Helvetica"],
      serif: `Georgia, Cambria, "Times New Roman", Times, serif`,
    },
    textColor: {
      title: "#ffffff",
      subtitle: "#BDBDBD",
      accent: "#e0a458",
      link: "#aaaaaa",
    },
    borderColor: {
      link: "#aaaaaa",
      accent: "#e0a458",
    },
    colors: {
      main: "#36383A",
      secondary: "#454757",
    },
  },
  variants: {},
  plugins: [],
}
