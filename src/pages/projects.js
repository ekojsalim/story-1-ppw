import React from "react"

import Header from "../components/header"
import SEO from "../components/seo"
import Img from "gatsby-image"
import tw from "tailwind.macro"

import { useStaticQuery, graphql } from "gatsby"

const ImageContainer = tw.div`w-full mb-4 md:mb-0 md:w-1/3 px-6 hover:cursor-pointer hover:shadow`

const ImageContainerBottom = tw(ImageContainer)`md:mx-8`

const ProjectsPage = () => {
  const query = useStaticQuery(graphql`
    fragment squareImage on File {
      childImageSharp {
        fluid(maxWidth: 500, maxHeight: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    query {
      linebot: file(relativePath: { eq: "projects/linebot.png" }) {
        ...squareImage
      }

      atrox: file(relativePath: { eq: "projects/atrox.png" }) {
        ...squareImage
      }

      utbk: file(relativePath: { eq: "projects/utbk.png" }) {
        ...squareImage
      }

      csgo: file(relativePath: { eq: "projects/csgo.png" }) {
        ...squareImage
      }

      lasor: file(relativePath: { eq: "projects/lasor.png" }) {
        ...squareImage
      }
    }
  `)
  return (
    <div css={tw`bg-main min-h-screen text-title flex flex-col`}>
      <Header title="projects" />
      <SEO title="Projects | Eko J. Salim" />
      <div
        css={tw`rounded-lg p-6 md:p-16 flex flex-wrap justify-center items-center flex-1`}
      >
        <div css={tw`flex flex-wrap justify-around w-full md:mb-8`}>
          <ImageContainer>
            <a href="http://polymorphism.xyz/analisis3.html">
              <Img fluid={query.utbk.childImageSharp.fluid} />
            </a>
          </ImageContainer>
          <ImageContainer>
            <a href="https://github.com/ekojsalim/atrox">
              <Img fluid={query.atrox.childImageSharp.fluid} />
            </a>
          </ImageContainer>
          <ImageContainer>
            <a href="https://github.com/ekojsalim/linebot">
              <Img fluid={query.linebot.childImageSharp.fluid} />
            </a>
          </ImageContainer>
        </div>
        <div css={tw`flex flex-wrap justify-center w-full`}>
          <ImageContainerBottom>
            <a href="https://www.reddit.com/r/GlobalOffensive/comments/8nrui6/analyze_your_mm_data_and_find_banned_players_from/">
              <Img fluid={query.csgo.childImageSharp.fluid} />
            </a>
          </ImageContainerBottom>
          <ImageContainerBottom>
            <a href="https://docs.google.com/presentation/d/1ZfOVhnq8RYrN8jwREb5lXw4MTX7wnmao3w2EPxq6QPM/edit?usp=sharing">
              <Img fluid={query.lasor.childImageSharp.fluid} />
            </a>
          </ImageContainerBottom>
        </div>
      </div>
    </div>
  )
}

export default ProjectsPage
