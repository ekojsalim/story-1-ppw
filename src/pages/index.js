import React from "react"

import { Link } from "gatsby"

import PerlinAnimation from "../components/anim"

import tw from "tailwind.macro"

const NavLinkLocal = tw(
  Link
)`text-subtitle font-heading tracking-wide font-medium hover:text-accent mx-4 text-link border-link hover:border-accent border-b mt-2`

const NavLink = tw.a`text-subtitle font-heading tracking-wide font-medium hover:text-accent mx-4 text-link border-link hover:border-accent border-b mt-2`

export default () => {
  return (
    <div css={tw`h-screen w-screen bg-main flex`}>
      <div css={tw`w-full md:w-1/2 flex items-center justify-center md:px-8`}>
        <div css={tw`flex-col max-w-full`}>
          <h1
            css={tw`font-heading text-5xl text-title font-extrabold uppercase tracking-wider mb-1 text-center`}
          >
            Eko Julianto Salim
          </h1>
          <h2
            css={tw`font-heading font-medium text-subtitle tracking-wide text-lg md:text-xl text-center italic mb-1`}
          >
            Student, Developer, and Data Science Enthusiast
          </h2>
          <nav css={tw`text-center text-lg flex flex-wrap justify-center`}>
            <NavLinkLocal to="/about">about</NavLinkLocal>
            <NavLinkLocal to="/projects">projects</NavLinkLocal>
            <NavLink href="https://blog.morphism.id">blog</NavLink>
            <NavLink href="https://www.linkedin.com/in/ekojsalim/">
              linkedin
            </NavLink>
            <NavLink href="https://github.com/ekojsalim">github</NavLink>
          </nav>
        </div>
      </div>
      <div css={tw`hidden md:block md:w-1/2`}>
        <PerlinAnimation />
      </div>
    </div>
  )
}
