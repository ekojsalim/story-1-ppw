import React from "react"
import Carousel, { Modal, ModalGateway } from "react-images"
import Layout from "../components/layout"
import tw from "tailwind.macro"

import SEO from "../components/seo"

const images = [
  {
    src:
      "https://images.unsplash.com/photo-1542608660-4ae68832767a?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjIxMTg3fQ",
  },
  {
    src:
      "https://images.unsplash.com/photo-1551022345-84679f5d61d5?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjIxMTg3fQ",
  },
  {
    src:
      "https://images.unsplash.com/flagged/photo-1561023368-e096c0febd85?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjIxMTg3fQ",
  },
]

export default class Component extends React.Component {
  state = { modalIsOpen: false }
  toggleModal = () => {
    this.setState(state => ({ modalIsOpen: !state.modalIsOpen }))
  }
  render() {
    const { modalIsOpen } = this.state

    return (
      <Layout title="gallery">
        <SEO title="Gallery of Lions | Eko J. Salim" />
        <div css={tw`flex justify-center items-center`}>
          <button
            onClick={() => this.toggleModal()}
            css={tw`text-accent border p-2`}
          >
            Open Gallery
          </button>
        </div>
        <ModalGateway>
          {modalIsOpen ? (
            <Modal onClose={this.toggleModal}>
              <Carousel views={images} />
            </Modal>
          ) : null}
        </ModalGateway>
      </Layout>
    )
  }
}
