import React from "react"

import Header from "./header"
import tw from "tailwind.macro"

import "./layout.css"

const Layout = ({ children, title }) => {
  return (
    <div css={tw`bg-main min-h-screen text-title`}>
      <Header title={title} />
      <div
        style={{
          margin: `0 auto`,
          maxWidth: 960,
          padding: `0 1.0875rem 1.45rem`,
        }}
      >
        <main>{children}</main>
      </div>
    </div>
  )
}

export default Layout
