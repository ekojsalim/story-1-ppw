import { Link } from "gatsby"
import React from "react"
import tw from "tailwind.macro"

import Image from "../components/image"

const Header = ({ title }) => (
  <header>
    <nav css={tw`flex justify-between py-6 px-6 md:px-12`}>
      <div css={tw`flex items-center`}>
        <Link to="/">
          <div css={tw`max-w-xs w-10`}>
            <Image />
          </div>
        </Link>
        <span css={tw`ml-2 text-accent font-medium text-xl`}>{title}</span>
      </div>
      <Link to="/" css={tw`flex`}>
        <div css={tw`max-w-xs w-10 flex justify-center items-center`}>
          <svg
            width="24"
            height="24"
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
          >
            <path d="M2.117 12l7.527 6.235-.644.765-9-7.521 9-7.479.645.764-7.529 6.236h21.884v1h-21.883z" />
          </svg>
        </div>
      </Link>
    </nav>
  </header>
)

export default Header
